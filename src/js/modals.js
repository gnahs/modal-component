import Modal from './modal'

class Modals {
    constructor(settings = {}) {
        this.modals = []

        this.defaultSettings = {
            selector: '[data-modal-content]',
        }

        this.settings = {
            ...this.defaultSettings,
            ...settings,
        }

        this.init()
    }

    init() {
        document.querySelectorAll(this.settings.selector).forEach((modal) => {
            this.modals.push(new Modal({
                id: modal.getAttribute('id'),
                // openClass: 'is-open',
                // onShow: () => { },
                // onClose: () => { },
            }))
        })
    }

    showModal(id) {
        const showModal = this.modals.filter(modal => modal.settings.id === id)
        if (showModal.length === 0) {
            return
        }
        showModal[0].show()
    }

    closeModal(id) {
        const hideModal = this.modals.filter(modal => modal.settings.id === id)
        if (hideModal.length === 0) {
            return
        }
        hideModal[0].hide()
    }

    /* Getters */
    getModals() {
        return this.modals
    }
}

export default Modals
