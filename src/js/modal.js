/* eslint-disable max-len */
import { h } from 'utils/utils'

class Modal {
    constructor(settings = {}) {
        // this.modals = []

        const defaultSettings = {
            id: '',
            selector: '#myModal',
            openTrigger: 'data-open-modal',
            closeTrigger: 'data-close-modal',
            openClass: 'is-open',
            overlayType: '',
            noEscape: false,
            onShow: () => {},
            onClose: () => {},
        }

        this.settings = {
            ...defaultSettings,
            ...settings,
        }

        this.modal = null

        this.offset = null

        this.events = {
            openModal: this.openModal.bind(this),
            closeModal: this.closeModal.bind(this),
            escapeModal: this.escapeModal.bind(this),
        }

        this.init()
    }

    init() {
        this.setId()
        this.createModalDOM()
        this.modal = document.querySelector(`.c-modal[data-id="${this.settings.id}"]`)
        this.attachOpenModalEvent()
        this.attachCloseModalEvent()
    }

    setId() {
        if (this.settings.id !== '') return

        if (this.settings.selector[0] === '#') {
            this.settings.id = this.settings.selector.split('#')[1]
            return
        }
        
        this.settings.id = Math.random().toString(36).substring(2, 15)
    }

    createModalDOM() {
        // h('div', { class: 'c-modal' })
        const body = document.querySelector('body')
        const modalContent = document.querySelector(this.settings.selector)
        modalContent.removeAttribute('data-modal-content')
        const modalContainer = h('div', { class: 'c-modal__container', 'aria-modal': true, role: 'dialog' }, modalContent)
        const modalOverlay = h('div', { class: `c-modal__overlay ${this.settings.overlayType}`, tabindex: '-1', 'data-close-modal': true }, modalContainer)
        const modal = h('div', { class: 'c-modal', 'aria-hidden': true, 'data-id': this.settings.id }, modalOverlay)
        body.append(modal)
    }

    attachCloseModalEvent() {
        this.modal.addEventListener('click', this.events.closeModal, true)
    }

    attachOpenModalEvent() {
        window.addEventListener('click', this.events.openModal, true)
    }


    /* Methods */
    show() {
        this.modal.classList.add('is-open')
        if (this.settings.openClass !== 'is-open') {
            this.modal.classList.add(this.settings.openClass)
        }
        this.modal.setAttribute('aria-hidden', false)
        window.addEventListener('keydown', this.events.escapeModal, true)

        this.offset = window.pageYOffset
        document.body.style.top = `${parseInt(-this.offset, 10)}px`
        document.body.classList.add('modal-opened')

        this.settings.onShow()
    }

    hide() {
        const hidemodal = this.modal
        this.modal.setAttribute('aria-hidden', true)
        this.modal.addEventListener('animationend', function handler() {
            hidemodal.classList.remove('is-open')
            hidemodal.removeEventListener('animationend', handler, false)
        }, false)
        window.removeEventListener('keydown', this.events.escapeModal, true)

        document.body.classList.remove('modal-opened')
        window.scrollTo(0, this.offset)
        document.body.style.top = ''

        this.settings.onClose()
    }

    /* Events */
    closeModal(ev) {
        if (!ev.target.hasAttribute('data-close-modal')) {
            return
        }
        this.hide()
        if (this.settings.openClass !== 'is-open') {
            this.modal.classList.remove(this.settings.openClass)
        }
    }

    openModal(ev) {
        if (!ev.target.hasAttribute('data-open-modal') || this.settings.selector !== ev.target.getAttribute('data-open-modal')) {
            return
        }
        this.show()
    }

    escapeModal(ev) {
        if (this.settings.noEscape) {
            return
        }
        ev.preventDefault()
        if (ev.keyCode === 27) {
            this.hide()
        }
    }

    // /* Getters */
    // getModals() {
    //     return this.modals
    // }

    // getModal(selector) {
    //     const filtered = this.modals.filter(modal => modal.container === selector)

    //     return filtered
    // }
}

export default Modal
