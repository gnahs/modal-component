// import 'babel-polyfill'
import { ForEach } from 'js-polyfills/src/js-polyfills'
import '../css/modal.css'
import Modal from '../js/modal'

// window.gnaModals = new Modals({ selector: '.is-modal' })

window.gnaModal = new Modal({
    selector: '.is-modal1',
    openClass: 'is-open',
    openTrigger: 'data-open-modal',
    closeTrigger: 'data-close-modal',
    overlayType: 'transparent',
    onShow: () => { console.log('holaaa') },
    onClose: () => { console.log('adeuuuu') },
})
